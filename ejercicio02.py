""" Utilizando diccionarios diseñar un programa modular que permita gestionar los productos de un comercio,
    las funcionalidades solicitadas son:
    a. Registrar productos: para cada uno se debe solicitar, código, descripción, precio y stock. Agregar las
    siguientes validaciones:
        i. El código no se puede repetir
        ii. Todos los valores son obligatorios
        iii. El precio y el stock no pueden ser negativos
    b. Mostrar el listado de productos
    c. Mostrar los productos cuyo stock se encuentre en el intervalo [desde, hasta]
    d. Diseñar un proceso que le sume X al stock de todos los productos cuyo valor actual de stock sea menor
        al valor Y.
    e. Eliminar todos los productos cuyo stock sea igual a cero.
    f. Salir """
import os
## (a) Funcion para Registrar Productos y validando cada uno de los campos
def registrarProductos(producto):
    while True:
        try:
            l=True
            while l==True:
                os.system('cls')
                codigo=noRepetir(producto)
                if codigo>0:
                    descripcion=validarDescripcion()
                    precio=validarPrecio()
                    print("Ingrese un stock para el producto")
                    stock=validarNumPositivo()
                    producto[codigo]=[descripcion,precio,stock]
                    
                elif codigo==0:
                    l=False
                    return producto
                else:
                    l=True
                    print("No se admiten valores negativos o 0")
        except ValueError:
            print("Debe ingresar un código de numero positivo entero")

def noRepetir(prod):
    l=True
    while l==True:
        print("Ingrese un código para el producto. Ingrese 0 para no añadir mas")
        cod=int(input())
        if cod in prod.keys():
            os.system('cls')
            print("El código ingresado ya existe")
        else:
            l==False
            return cod

##(a.1) Subfunción para validar el Precio
def validarPrecio():
    while True:
        try:
            l=True
            while l==True:
                pre=float(input("Ingrese el precio: "))
                if pre>0:
                    l=False
                    return pre
                else:
                    l==True
                    print("Debe ingresar un Precio válido. No negativos o 0")
        except ValueError:
            print("Debe ser un número")
##(a.2) Subfunción para validar que la descripción no sea espacio vacío
def validarDescripcion():
    l=True
    while l==True:
        desc=input("Ingrese la descripción: ")
        if bool(desc)==True and desc.isspace()==False:
            l=False
            return desc
        else:
            l==True
            print("Es obligatorio una descripcion")
## (10) Subfunción para validar que el numero ingresado no sea negativo
def validarNumPositivo():
    while True:
        try:
            l=True
            while l==True:
                stk=int(input())
                if stk>=0:
                    l=False
                    return stk
                else:
                    l==True
                    print("Debe ingresar un Stock válido. No negativos")
        except ValueError:
            print("Debe ingresar un número")


## (b)Funcion para mostrar todos los productos
def mostrarProductos(prod):
    for codigo, datos in prod.items():
        print("El codigo:", codigo,"|Tiene descripcion:",datos[0], "|con el precio $",datos[1],"|con stock de",datos[2])

## (c) Funcion para mostrar los productos en un intervalo [x,y]
def mostrarStock(prod):
    print("Ingrese desde y hasta cuanto numeros de stock desea ver: ")
    i=validarNumPositivo()
    j=validarNumPositivo()
    if i<j:
        buscarProd(i,j,prod)
        
    elif i>j:
        buscarProd(j,i,prod)
        
    elif i==j:
        print("Los números ingresados es un mismo número, no es un intervalo")
        n=i
        for codigo,datos in prod.items():
                if datos[2]==n:
                    print("El codigo:", codigo,"|Tiene descripcion:",datos[0], "|con el precio $",datos[1],"|con stock de",datos[2])
## (c.1) Subfuncion para ordenar los num i,j y mostrar los productos
def buscarProd(x,y,product):
    for n in range(x,y+1):
        array=[]
        array.append(n)
        lista=array
        cont=0
        for codigo,datos in product.items():
            if datos[2] in lista:
                print("El codigo:", codigo,"|Tiene descripcion:",datos[0], "|con el precio $",datos[1],"|con stock de",datos[2])
        


## (d) Funcion para sumar el stock del producto
def sumarValor(prod):
    print("Ingrese el stock maximo el cual desea buscar")
    buscar=validarNumPositivo()
    print("Ingrese el valor a sumar: ")
    sumar=validarNumPositivo()
    for codigo,datos in prod.items():
                if datos[2]<buscar:
                    datos[2]=datos[2]+sumar
                    print(datos)

## (e) Funcion para eliminar los stock en 0
def eliminarStockCero(prod):
    for codigo,datos in prod.items():
        if datos[2]==0:
            del prod[codigo]
            return prod


def menu():
    while True:
        try:
            print("1 - Registrar productos\n"
                  "2 - Mostrar el listado de productos\n"
                  "3 - Mostrar los productos cuyo stock se encuentre en el intervalo [desde, hasta]\n"
                  "4 - Sumar X al stock de todos los productos cuyo valor actual de stock sea menor al valor Y.\n"
                  "5 - Eliminar todos los productos cuyo stock sea igual a cero.\n"
                  "6 - Salir")
            t=int(input())
            os.system('cls')
            return t
        except ValueError:
            os.system('cls')
            print("Opción no válida. Intente otra vez...")

product={}
l=True
os.system('cls')
while l:
    op=menu()
    if op==1:
        registrarProductos(product)
    elif op==2:
        mostrarProductos(product)
    elif op==3:
        mostrarStock(product)
    elif op==4:
       sumarValor(product)
    elif op==5:
       eliminarStockCero(product)
    elif op==6:
        l=False
        print("Saliendo del programa...")

    else:
        print("Has ingresado una opcion incorrecta")